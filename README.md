# Projet VitalEATy  

## Contexte

Je suis étudiant qui rentre en 3ème année de BUT Informatique, j'ai commencé ce projet dans le but d'apprendre React.js et son framework Next.js.

## Projet

 Une application basée sur la nutrition et la santé via l'alimentation. Cette application aura plusieurs modules comme (liste vouée à évoluer dans le temps): 
- Le calcul de l'IMC
- La recherche de produits alimentaires du commerce via des mots clés avec une comparaison facile des valeurs nutritionnelles. Ajout de filtres ...
- Le calcul des valeurs nutritionnelles de vos repas : Sélectionner vos aliments et leur quantité pour savoir les valeurs nutritionnelles de l'ensemble de votre repas. J'aimerais y inclure une sauvegarde des recettes (Systeme de compte utilisateur ? Systeme de sauvegarde dans le localStorage ? (disparait si on vide le cache ..) / Exportation des recettes format pdf ou autres ?)
- Des informations relatives aux nutriments et potentiellement sur les additifs

Utilisation de l'API d'openFoodFacts me permettant d'accéder à leur base de données pour l'élaboration de ce projet. Merci à eux !


![OpenFoodFacts image](https://blog.openfoodfacts.org/wp-content/uploads/2022/05/EXE_LOGO_OFF_RVB_Plan-de-travail-1-copie-0-1024x341.jpg)

## Particularité 

Utilisation de la librairie i18n pour une version en français et en anglais

## Technologies  

- Next.js
- React.js
- JavaScript
- JSX
- Redux
- i18n
- Tailwind CSS

---
# Next.js Readme :

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.js`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
