import React from "react";
import ImcCalculator from "../app/Components/ImcCalculator/ImcCalculator";
import i18n from "../app/i18n";
import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";
import store from "../app/redux/store";

const Home = () => {
  return (
    <ImcCalculator />
  );
};

export default Home;
