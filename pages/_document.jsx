import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <link rel="apple-touch-icon" href="logo.png" sizes="192x192" />
          <link rel="icon" href="logo.png" />
        </Head>
        <body>
          <Main />
          
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
