import "tailwindcss/tailwind.css";
import { I18nextProvider } from "react-i18next";
import i18n from "../app/i18n";
import { Provider } from "react-redux";
import store from "../app/redux/store";
import SideMenu from "../app/Components/SideMenu/SideMenu";

function MyApp({ Component, pageProps }) {
  return (
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <div className="App flex h-full">
          <SideMenu />
          <div className={`flex-col w-85p`}>
            <Component {...pageProps} />
          </div>
        </div>
      </Provider>
    </I18nextProvider>
  );
}

export default MyApp;
