import React, { ChangeEvent, useState } from "react";
import { useTranslation } from "react-i18next";
import { Language } from "../../enums/Language.ts";
import "../../globals.css";
import Image from "next/image";
import franceFlag from "../../res/img/flags/france.svg";
import gbFlag from "../../res/img/flags/gb.svg";


const Lang = () => {
  const { i18n } = useTranslation();
  const [lang, setLang] = useState<Language>(i18n.language as Language);

  let changeLanguage = (language: string) => {
    switch (language) {
      case Language.EN:
        setLang(Language.EN);
        i18n.changeLanguage(Language.EN);
        break;
      case Language.FR:
        setLang(Language.FR);
        i18n.changeLanguage(Language.FR);
        break;
    }
  };

  return (
    <div className="flex gap-2">
      <div
        className="hover:cursor-pointer"
        onClick={() => changeLanguage("fr")}
      >
        <Image      
        src={franceFlag}
        width={24}
        height={24}
        alt="France flag"/>
      </div>

      <div
        className="hover:cursor-pointer"
        onClick={() => changeLanguage("en")}
      >
        <Image      
        src={gbFlag}
        width={24}
        height={24}
        alt="France flag"/>
      </div>
    </div>
  );
};

export default Lang;
