"use client";

import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import { setData } from "../../redux/dataSlice.js";
import data from "./dataFoodAnalysis.js";
import  nsNoData  from "../../res/img/nutriScore/nodata.png";
import Image from "next/image.js";

const NutritionalValues = (props) => {
  const { t } = useTranslation();
  const products = useSelector((state) => state.data.products);

  let nutriScoreImage = nsNoData;
  let nutriscore = "no data";

  const product = products?.find((product) => product.code === props.code);

  const renderNutriment = (nutrimentType, idx) => {
    const nutrimentData = data[nutrimentType];
    const nutrimentAPI = nutrimentData.nutrimentAPI;
    const suffixes = nutrimentData.suffix;
    const color = nutrimentData.color[0];

    const formattedNutriments = nutrimentAPI.map(
      (nutrimentAPIKey, index) => {
        const value = product.nutriments[nutrimentAPIKey];
        const suffix = suffixes[index];

        const formattedValue = value ? `${value} ${suffix}` : "X";

        return (
          <span key={index}>
            {index !== 0 && " / "}
            {formattedValue}
          </span>
        );
      }
    );

    if (product.nutrition_grades !== undefined) {
      nutriScoreImage = `/nutriScore/${product.nutrition_grades.toUpperCase()}.png`;
      nutriscore = product.nutrition_grades.toUpperCase();
    }

    return (
      <div key={idx}
        className={`NutritionalValues flex flex-row items-center justify-between gap-2 p-1 ${color}`}
      >
        <div className="label">{t(`common.${nutrimentType}`)}</div>
        <div className="nutrimentsValues">{formattedNutriments}</div>
      </div>
    );
  };

  return (
    <div className="nutritionValuesCtn flex flex-col items-center p-2 rounded-lg font-bold lg:text-xs text-[0.5rem]">
      <div className="rounded overflow-hidden flex flex-col">
        <div
          id="labelAgvNV"
          className="min-h-[1.75rem] p-1 text-slate-950  overflow-hidden text-center flex flex-row items-center justify-center mb-4"
        >
          {t("common.AvgNV")} {t("common.for100g")}
        </div>

        {Object.keys(data).map((nutrimentType,index) =>
          renderNutriment(nutrimentType,index)
        )}
        <Image className="self-center mt-2" src={nutriScoreImage} width={96} height={48} alt={`nutri score image ${nutriscore}`}/>
      </div>
      <div id="legend" className="mt-2">
        X = {t("common.noData")}
      </div>
    </div>
  );
};

export default NutritionalValues;
