import React from "react";
import { useTranslation } from "react-i18next";

const Filter = () => {
    const { t } = useTranslation();
    const currentLanguage = t("common.translate-button");
  return (
    <div className="filtersCtn flex justify-center w-full min-h-[75px] items-center max-xl:gap-8 border-b border-black">
      {/* SORT BY */}

        <select name="sortBy" id="sort-select" className="rounded">
          <option value="">{t("filters.popularity")}</option>
          <option value="dog">{t("filters.ecoScore")}</option>
          <option value="cat">Cat</option>
          <option value="hamster">Hamster</option>
          <option value="parrot">Parrot</option>
          <option value="spider">Spider</option>
          <option value="goldfish">Goldfish</option>
        </select>


      {/* FILTER */}
    </div>
  );
};

export default Filter;
