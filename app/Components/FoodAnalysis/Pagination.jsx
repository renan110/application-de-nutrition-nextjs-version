import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

const Pagination = ({
  currentPage,
  nbPages,
  onPreviousPage,
  onNextPage,
  onChosenPage,
  productNotFound,
}) => {
  const { t } = useTranslation();
  const currentLanguage = t("common.translate-button");

  const [visiblePages, setVisiblePages] = useState([]);
  console.log(
    `current page ${currentPage} , nombre page ${nbPages} , visible pages ${visiblePages}`
  );

  const handleVisiblePages = () => {
    if (
      currentPage === 1 ||
      currentPage === 2 ||
      currentPage === nbPages - 1 ||
      currentPage === nbPages
    ) {
      setVisiblePages([1, 2, 3, nbPages - 2, nbPages - 1, nbPages]);
    } else {
      const set = new Set();
      set.add(1);
      set.add(2);
      set.add(3);
      setVisiblePages();
    }
  };

  const initializeSetVisiblesPages = () => {
    const set = new Set();
    set.add(1);
    if (nbPages >= 2) {
      set.add(2);
    }
    if (nbPages >= 3) {
      set.add(3);
    }

    if (nbPages - 2 > 0) {
      set.add(nbPages - 2);
    }
    if (nbPages - 1 > 0) {
      set.add(nbPages - 1);
    }
    set.add(nbPages);
    return set;
  };

  const getVisiblePageNumbers = () => {
    const totalPages = nbPages;
    const currentPageNum = currentPage;
    const visiblePageNumbers = [];

    const maxTotalVisiblePages = 9; // Le nombre maxtotal de boutons visibles
    const numStartEndButtons = 3; // Le nombre de boutons pour les pages 1, 2 et 3 et les 3 dernières pages

    // Cas où il n'y a pas assez de pages pour afficher les points de suspension
    if (totalPages <= maxTotalVisiblePages) {
      for (let i = 1; i <= totalPages; i++) {
        visiblePageNumbers.push(i);
      }
    } else {
      // Ajouter les boutons pour les pages 1, 2 et 3
      for (let i = 1; i <= numStartEndButtons; i++) {
        visiblePageNumbers.push(i);
      }

      const middleStart = Math.max(numStartEndButtons + 1, currentPageNum - 1);
      const middleEnd = Math.min(
        totalPages - numStartEndButtons,
        currentPageNum + 1
      );
      console.log(middleStart, middleEnd);

      for (let i = middleStart; i <= middleEnd; i++) {
        visiblePageNumbers.push(i);
        console.log(i);
      }

      // Ajouter les boutons pour les 3 dernières pages
      for (let i = totalPages - numStartEndButtons + 1; i <= totalPages; i++) {
        visiblePageNumbers.push(i);
      }
    }

    console.log(visiblePageNumbers);

    return visiblePageNumbers;
  };

  return (
    <div className="Pagination mt-10 shadow-lg max-w-2xl self-center rounded-lg p-2 bg-neutral-100">
      {/* pagination */}
      {!productNotFound && (
        <div className="paginationCtn flex justify-center">
          {/* Bouton pour la page précédente */}
          <button
            className="mx-2 my-1 px-1 py-2 cursor-pointer border-solid text-black hover:bg-gray-800 hover:text-white bg-neutral-100 rounded"
            onClick={onPreviousPage}
            disabled={currentPage === 1}
            style={{ display: currentPage === 1 ? "none" : "block" }}
          >
            {"<<"}
          </button>

          {/* Afficher les numéros de page */}
          {getVisiblePageNumbers().map((pageNum, index, array) => {
            // Afficher le bouton de la page actuelle
            if (pageNum === currentPage) {
              return (
                <button
                  key={pageNum}
                  onClick={() => onChosenPage(pageNum)}
                  disabled={true}
                  className="mx-2 my-1 px-1 py-2 border-solid text-white bg-gray-800 rounded"
                >
                  {pageNum}
                </button>
              );
            }

            // Afficher les points de suspension entre deux boutons de page
            if (index > 0 && array[index - 1] !== pageNum - 1) {
              return (
                <div className="flex">
                <span
                  key={`ellipsis-${pageNum}`}
                  className="flex flex-row items-center font-bold"
                >
                  ...
                </span>
                <button
                key={pageNum}
                className="mx-2 my-1 px-1 py-2 cursor-pointer border-solid text-black hover:bg-gray-800 hover:text-white bg-neutral-100 rounded"
                onClick={() => onChosenPage(pageNum)}
              >
                {pageNum}
              </button>
                </div>
                
              );
            }

            // Afficher les autres numéros de page
            return (
              <button
                key={pageNum}
                className="mx-2 my-1 px-1 py-2 cursor-pointer border-solid text-black hover:bg-gray-800 hover:text-white bg-neutral-100 rounded"
                onClick={() => onChosenPage(pageNum)}
              >
                {pageNum}
              </button>
            );
          })}

          {/* Bouton pour la page suivante */}
          <button
            className="mx-2 my-1 px-1 py-2 cursor-pointer border-solid text-black hover:bg-gray-800 hover:text-white bg-neutral-100 rounded"
            onClick={onNextPage}
            disabled={currentPage === nbPages}
            style={{ display: currentPage === nbPages ? "none" : "block" }}
          >
            {">>"}
          </button>
        </div>
      )}
    </div>
  );
};

export default Pagination;
