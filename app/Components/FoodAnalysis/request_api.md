https://world.openfoodfacts.org/cgi/search.pl?page_size=10&search_tag=en:vegetarian&tag_contains_0=contains&tag_0=en:vegetarian&tag_contains_1=does_not_contain&tag_1=en:vegetarian-status-unknown&ingredients_analysis_tags=en:vegetarian&ingredients_from_or_that_may_be_from_palm_oil=without&additives=without&ingredients_that_may_be_from_palm_oil=without&ingredients_from_palm_oil=without&stores=Carrefour%20City&search_simple=1&action=process&json=1&search_terms=Steak

https://world.openfoodfacts.org/cgi/search.pl?action=process&tagtype_0=categories&tag_contains_0=contains&tag_0=jams&nutriment_0=carbohydrates&nutriment_compare_0=lt&nutriment_value_0=30&sort_by=unique_scans_n&page_size=20 

http://world.openfoodfacts.org/cgi/search.pl?action=process&json=true&nutriment_0=carbohydrates&nutriment_compare_0=gte&nutriment_value_0=16&nutriment_1=proteins&nutriment_compare_1=gte&nutriment_value_1=16&nutriment_2=fat&nutriment_compare_2=gte&nutriment_value_2=16&nutriment_3=salt&nutriment_compare_3=lte&nutriment_value_3=10&tagtype_0=labels&tag_contains_0=contains&tag_0=organic

https://world.openfoodfacts.org/cgi/search.pl?action=process&tagtype_0=creator&tag_contains_0=contains&tag_0=usda-ndb-import&nutriment_0=nutrition-score-uk&nutriment_compare_0=gt&nutriment_value_0=0&sort_by=unique_scans_n&page_size=20&axis_x=energy-kj&axis_y=products_n&action=display

## gruyere - nutriscore C - score < 4
https://world.openfoodfacts.org/cgi/search.pl?search_terms=gruyere%20france&nutrition_grades=c&nutriment_0=nutrition-score-fr&nutriment_compare_0=gt&nutriment_value_0=4&search_simple=1&action=process&page=1&json=1

## If you want to go faster, you can search for waters that are not flavored but contains more than 0.5 sugar:
https://world.openfoodfacts.org/cgi/search.pl?action=process&tagtype_0=categories&tag_contains_0=contains&tag_0=waters&tagtype_1=categories&tag_contains_1=does_not_contain&tag_1=flavored%20waters&nutriment_1=sugars&nutriment_compare_1=gte&nutriment_value_1=0.5&sort_by=unique_scans_n&page_size=20&axis_x=energy-kj&axis_y=products_n&action=display 

## catégorie E pour moins de 20kcal 
https://fr.openfoodfacts.org/cgi/search.pl?action=p...ess&tagtype_0=nutrition_grades&tag_contains_0=contains&tag_0=E&nutriment_0=energy-kcal&nutriment_compare_0=lt&nutriment_value_0=20&sort_by=unique_scans_n&page_size=20&action=display

## ingredient_n 
https://fr.openfoodfacts.org/cgi/search.pl?action=process&tagtype_0=categories&tag_contains_0=contains&tag_0=miels&nutriment_0=ingredients_n&nutriment_compare_0=gt&nutriment_value_0=1&sort_by=unique_scans_n&page_size=20&page=1

## API V2 en developpement , ne supporte pas search terms
https://world.openfoodfacts.org/api/v2/search?categories_tags=en:sodas&energy-kj_100g%3C200&fields=code,product_name,energy-kj_100g

## tag v2 ?
https://be-en.openfoodfacts.org/api/v2/search?ingredients_analysis_tags=en:vegan&sort_by=created_t&page_size=10&page=1&search_terms=brood

## fish omega 3
https://world.openfoodfacts.org/cgi/search.pl?action=process&tagtype_0=categories&tag_contains_0=contains&tag_0=fish&nutriment_0=omega-3-fat&nutriment_compare_0=gt&nutriment_value_0=0&sort_by=unique_scans_n&page_size=20&axis_x=omega-3-fat