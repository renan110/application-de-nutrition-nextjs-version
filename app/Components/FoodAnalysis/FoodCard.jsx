import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import SearchMenu from "./SearchMenu";
import NutritionalValues from "./NutritionalValues";

const FoodCard = (props) => {
  const { t } = useTranslation();
  const currentLanguage = t("common.translate-button");

  const handleImageError = (event) => {
    event.target.src = "https://www.bricozor.com/static/img/visuel-indisponible-650.png";
  };

  return (
    <div className="productCardCtn flex flex-col p-2 gap-2 mt-5 rounded-lg justify-self-center w-full h-full bg-neutral-100 shadow-lg">
      <div id="productName" className="font-bold text-center uppercase min-h-[48px] text-xs lg:text-sm">
        {props.product.product_name_fr}
      </div>

      <div className="imgCtn flex justify-center 2xl:h-24 2xl:w-24 xl:h-16 xl:w-16 h-12 w-12
       self-center">
      {props.product.image_url != undefined ? (
        <img
          className="imgProduct h-full text-center object-contain rounded-lg"
          src={props.product.image_url}
          alt={t("common.noImage")}
          onError={handleImageError}
        />
      ) : (
        <img
          className="imgProduct h-full w-80p text-center object-contain rounded-lg"
          src="https://www.bricozor.com/static/img/visuel-indisponible-650.png"
          alt={t("common.noImage")}
        />
      )}
      </div>

      <NutritionalValues 
        key={props.product.code}
        code={props.product.code}
      />
    </div>
  );
  
};

export default FoodCard;
