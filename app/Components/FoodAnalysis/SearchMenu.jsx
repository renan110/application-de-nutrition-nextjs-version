import React, { useState, useEffect , useRef } from "react";
import { useTranslation } from "react-i18next";
import FoodCard from "./FoodCard.jsx";
import { useSelector, useDispatch } from "react-redux";
import { setData } from "../../redux/dataSlice.js";
import { TbFaceIdError } from "react-icons/tb";
import {
  GiMilkCarton,
  GiFruitBowl,
  GiTomato,
  GiChipsBag,
  GiBowlOfRice,
  GiMeat,
} from "react-icons/gi";
import Pagination from "./Pagination";
import { AiOutlineSearch } from "react-icons/ai";
import Filter from "./Filter.jsx";

const SearchMenu = () => {
  const { t } = useTranslation();
  const currentLanguage = t("common.translate-button");

  const [searchValue, setSearchValue] = useState("");
  const nutritionData = useSelector((state) => state.data);
  const [productNotFound, setProductNotFound] = useState(false);
  const [nbPages, setNbPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);

  // FILTERS STATES
  const [orderBy, setOrderBy] = useState("");


  const dispatch = useDispatch();
  
  const buttonRef = useRef(null);

  // Fonction de gestion de la recherche
  const handleFetch = async (page) => {
    window.scrollTo(0, 0);
    // Mettre anglais / francais
    try {
      const response = await fetch(
        `https://world.openfoodfacts.org/cgi/search.pl?search_terms=${searchValue}&search_simple=1&action=process&page=${page}&json=1&`
      );
      const data = await response.json();
      dispatch(setData(data.products));
      setNbPages(Math.ceil(data.count / 24));
    } catch (error) {
      console.error("Erreur lors de la recherche :", error);
      setProductNotFound(true);
    }
  };

  const handleSearch = async () => {
    setCurrentPage(1);
    handleFetch(1);
  };

  const handleNextPage = async () => {
    if (currentPage < nbPages) {
      setCurrentPage(currentPage + 1);
      handleFetch(currentPage + 1);
    }
  };

  const handlePreviousPage = async () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
      handleFetch(currentPage - 1);
    }
  };

  const handleChosenPage = async (chosenPage) => {
    if (chosenPage >= 0 && chosenPage <= nbPages) {
      setCurrentPage(chosenPage);
      handleFetch(chosenPage);
    }
  };

  const handleKeyDown = (event) => {
    // 13 = ENTER
    if(event.keyCode === 13){
      buttonRef.current.click();
    }
  }


  useEffect(() => {
    if (nutritionData) {
      setProductNotFound(
        !nutritionData.products || nutritionData.products.length === 0
      );
    }
  }, [nutritionData, currentPage]);

  return (
    <div className="SearchMenu flex flex-col min-h-screen" onKeyDown={handleKeyDown}>
      <div className="searchBarCtn flex justify-center bg-slate-700 w-full min-h-[75px] items-center rounded-b max-xl:gap-8">
        <label className="labelSearch flex flex-row text-white uppercase font-extrabold font-mono mr-auto ml-3 items-center max-xl:text-xs max-lg:text-xs max-lg:hidden">
          <div className="flex flex-row ml-3 mr-3">
            <GiMilkCarton />
            <GiFruitBowl />
            <GiTomato />
          </div>
          <span className="whitespace-nowrap">
            {t("foodAnalysis.searchProduct")}
          </span>
          <div className="flex flex-row ml-3 mr-3">
            <GiChipsBag />
            <GiBowlOfRice />
            <GiMeat />
          </div>
        </label>

        <div className="inputCtn flex mr-48 max-lg:mr-0 max-lg:text-xs">
          <input
            type="text"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            className="rounded-l h-8 self-center flex items-center outline-none appearance-none"
          />
          <button
            className="text-white bg-slate-500 h-8 self-center p-2 rounded-r flex items-center hover:bg-slate-900"
            ref={buttonRef}
            onClick={() => handleSearch()}
          >
            {/* {t("common.search")} */}
            <AiOutlineSearch />
          </button>
        </div>

        <div className="w-24 h-10 bg-slate-700 ml-auto max-lg:hidden"></div>
      </div>

     <Filter />

      <div className="cardsCtn grid 2xl:grid-cols-6 xl:grid-cols-4 lg:grid-cols-3 sm:grid-cols-2 gap-5 mx-2">
        {!productNotFound &&
          nutritionData &&
          nutritionData.products &&
          nutritionData.products.map((product) => (
            <FoodCard key={product.code} product={product} />
          ))}
      </div>

      {nbPages !== 0 && (
        <Pagination
          currentPage={currentPage}
          nbPages={nbPages}
          onPreviousPage={handlePreviousPage}
          onNextPage={handleNextPage}
          onChosenPage={handleChosenPage}
          productNotFound={productNotFound}
        />
      )}
    </div>
  );
};

export default SearchMenu;
