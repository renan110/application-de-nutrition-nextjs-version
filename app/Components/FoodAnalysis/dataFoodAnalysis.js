const data = {
    energy: {
        nutrimentAPI: ['energy-kj_100g', 'energy-kcal_100g'],
        suffix: ['kj ', 'kcal'],
        color: ['bg-slate-100 rounded-t-lg']
    },
    totalFat: {
        nutrimentAPI: ['fat_100g'],
        suffix: ['g'],
        color: ['bg-slate-200']
    },
    saturedFat: {
        nutrimentAPI: ['saturated-fat_100g'],
        suffix: ['g'],
        color: ['bg-slate-300']
    },
    totalCarbohydrates: {
        nutrimentAPI: ['carbohydrates_100g'],
        suffix: ['g'],
        color: ['bg-slate-400']
    },
    sugars: {
        nutrimentAPI: ['sugars_100g'],
        suffix: ['g'],
        color: ['bg-slate-500 text-white']
    },
    protein: {
        nutrimentAPI: ['proteins_100g'],
        suffix: ['g'],
        color: ['bg-slate-600 text-white']
    },
    salt: {
        nutrimentAPI: ['salt_100g'],
        suffix: ['g'],
        color: ['bg-slate-700 text-white rounded-b-lg']
    },
};
  
export default data;

