"use client";

import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import imcImg from "../../res/img/imc.jpg";
import bmiImg from "../../res/img/bmi.jpg";
import Image from "next/image";

const ImcCalculator = () => {
  const { t, i18n } = useTranslation(); 
  const currentLanguage = i18n.language;

  let imgElement = null;

  console.log(imgElement);

  if (currentLanguage === "fr") {
    imgElement = <Image className="h-96 w-auto rounded" src={imcImg} alt="Image IMC" />;
  } else if (currentLanguage === "en") {
    imgElement = <Image className="h-96 w-auto rounded" src={bmiImg} alt="Image BMI" />;
  }

  const [weight, setWeight] = useState("");
  const [heightCm, setHeightCm] = useState("");
  const [imc, setImc] = useState(null);
  const [entryError, setEntryError] = useState(false);
  const [imcColor, setImcColor] = useState("");

  const calculateImc = () => {
    const weightInKg = parseFloat(weight);
    const heightInM = parseFloat(heightCm) / 100;

    // A mettre dans utils 
    if (weightInKg > 1 && weightInKg < 300 && heightInM > 1 && heightCm < 250) {
      const imcValue = weightInKg / (heightInM * heightInM);
      setImc(imcValue.toFixed(2));
      setEntryError(false);

      // faire classe dynamique
      if (imcValue < 18.5 && currentLanguage === "fr") {
        setImcColor("bg-[#00CFC1]");
        return;
      } else if (imcValue < 18.5 && currentLanguage === "en") {
        setImcColor("bg-[#1C46AD]");
        return;
      } else if (imcValue < 24.9) {
        setImcColor("bg-[#02AD02]");
        return;
      } else if (imcValue < 29.9) {
        setImcColor("bg-[#FFB404]");
        return;
      } else if (imcValue < 34.9) {
        setImcColor("bg-[#FF7B04]");
        return;
      } else if (imcValue > 35) {
        setImcColor("bg-[#C90000]");
        return;
      }
    } else {
      setImc(null);
      setEntryError(true);
    }
  };

  
  return (
    <div className="ImcCalculator"> 
      <h2 className="font-bold text-3xl uppercase text-center m-3">{t("common.BmiCalculator")}</h2>

      <div className="flex flex-col gap-5 items-center">
        {imgElement}
        <div className="flex flex-col gap-3">
          <div className="flex">
            <label className="w-[200px]">{t("common.weight")}:</label>
            <input 
              placeholder="1-300kg"
              type="number"
              value={weight}
              onChange={(e) => setWeight(e.target.value)}
              className="rounded"
            />
          </div>
          <div>
            <div className="flex">
              <label className="w-[200px]">{t("common.height")}:</label>
              <input
                placeholder="100-250cm"
                type="number"
                value={heightCm}
                onChange={(e) => setHeightCm(e.target.value)}
                className="rounded"
              />
            </div>
          </div>
          <button className="rounded bg-slate-700 text-white p-1 hover:bg-slate-800" onClick={calculateImc}>{t("common.calculate")}</button>
          {imc && (
            <p className={`${imcColor} text-white font-bold text-center p-2 rounded`}>
              {t("common.yourBmi")} {imc}
            </p>
          )}

          {entryError && (
            <div className="p-2 rounded bg-red-900 text-white">{t("common.entryError")}</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ImcCalculator;
