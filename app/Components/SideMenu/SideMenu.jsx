"use client";

import React, { useState } from "react";
import Image from "next/image";
import logo from "../../res/img/logo.svg";
import logoPNG from "../../res/img/logo.png";
import { useTranslation } from "react-i18next";
import Lang from "../Langage/Lang";
import { appName } from "../../Data";
import { setActiveModule } from "../../redux/dataSlice.js";

import { RiBodyScanFill } from "react-icons/ri";
import { IoIosNutrition } from "react-icons/io";
import { GiMeal } from "react-icons/gi";
import { SiMoleculer } from "react-icons/si";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";

const SideMenu = () => {
  const { t, i18n } = useTranslation();

  const activeModule = useSelector((state) => state.data.activeModule);

  const dispatch = useDispatch();
  console.log(activeModule);

  const indexModule = {
    ImcCalculator: 1,
    FoodAnalysis: 2,
    Meal: 3,
    Nutrients: 4,
  };

  return (
    <aside className="SideMenu flex flex-col min-h-screen px-5 py-3 overflow-y-auto bg-white border-r rtl:border-r-0 rtl:border-l dark:bg-gray-900 dark:border-gray-700 w-15p min-w-[200px]">
      <div className="flex items-center border-b border-slate-800">
        <Image src={logoPNG} alt="logo" width={85} />
        <span className="font-bold text-[#7f7e4d]">{appName}</span>
      </div>

      <div className="flex flex-col justify-between flex-1 mt-6">
        <nav className="-mx-3 space-y-6 ">
          <div className="space-y-3 ">
            <label className="px-3 text-xs text-gray-500 uppercase dark:text-gray-400">
              {t("sideMenu.Functionality")}
            </label>

            {/* IMC */}
            <Link
              className="flex items-center px-3 py-2 text-gray-600 transition-colors duration-300 transform rounded-lg dark:text-gray-200 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:text-gray-200 hover:text-gray-700"
              href="imc"
              onClick={() =>
                dispatch(setActiveModule(indexModule.ImcCalculator))
              }
            >
              <div className="flex h-[36px] w-1/4 items-center content-center">
                <RiBodyScanFill className="h-[24px] w-[24px]" />
              </div>

              <span className="mx-2 text-sm font-medium w-3/4">
                {t("sideMenu.BMI")}
              </span>
            </Link>

            <Link
              className="flex items-center px-3 py-2 text-gray-600 transition-colors duration-300 transform rounded-lg dark:text-gray-200 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:text-gray-200 hover:text-gray-700"
              href="foodProduct"
              onClick={() =>
                dispatch(setActiveModule(indexModule.FoodAnalysis))
              }
            >
              <div className="flex h-[36px] w-1/4 items-center content-center">
                <IoIosNutrition className="h-[24px] w-[24px]" />
              </div>

              <span className="mx-2 text-sm font-medium w-3/4">
                {t("sideMenu.analyzeFood")}
              </span>
            </Link>

            <Link
              className="flex items-center px-3 py-2 text-gray-600 transition-colors duration-300 transform rounded-lg dark:text-gray-200 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:text-gray-200 hover:text-gray-700"
              href="#"
              onClick={() => dispatch(setActiveModule(indexModule.Meal))}
            >
              <div className="flex h-[36px] w-1/4 items-center content-center">
                <GiMeal className="h-[24px] w-[24px]" />
              </div>

              <span className="mx-2 text-sm font-medium w-3/4">
                {t("sideMenu.meal")}
              </span>
            </Link>

            <Link
              className="flex items-center px-3 py-2 text-gray-600 transition-colors duration-300 transform rounded-lg dark:text-gray-200 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:text-gray-200 hover:text-gray-700"
              href="#"
              onClick={() => dispatch(setActiveModule(indexModule.Nutrients))}
            >
              <div className="flex h-[36px] w-1/4 items-center content-center">
                <SiMoleculer className="h-[24px] w-[24px]" />
              </div>

              <span className="mx-2 text-sm font-medium w-3/4">
                {t("sideMenu.nutritionInfo")}
              </span>
            </Link>
          </div>

          <div className="flex">
            <label className="px-3 text-xs text-gray-500 uppercase dark:text-gray-400 mt-1">
              {t("sideMenu.langage")}
            </label>

            <Lang />
          </div>
        </nav>
      </div>
    </aside>
  );
};

export default SideMenu;
