import React, { useState } from "react";
import Lang from "../Langage/Lang.tsx";
import { useTranslation } from "react-i18next";
import { navLinksFr, navLinksEn } from "../../Data.js";

const NavBar = () => {
  const { t } = useTranslation();
  const currentLanguage = t("common.translate-button");

  let navLinks = navLinksFr;
  if (currentLanguage === "FR") {
    navLinks = navLinksFr;
  } else if (currentLanguage === "EN") {
    navLinks = navLinksEn;
  }

  return (
    <div className="container bg-slate-700 flex justify-between items-center h-12 min-w-full">
      <div className="flex text-white">
        {navLinks.map((link) => {
          return (
            <div className="hover:cursor-pointer" key={link.id}>
              <p>{link.name}</p>
            </div>
          );
        })}
      </div>

      <div className="hover:cursor-pointer">
        <Lang />
      </div>
    </div>
  );
};

export default NavBar;
