import { createSlice } from '@reduxjs/toolkit';

const dataSlice = createSlice({
  name: 'data',
  initialState: {
    products: null, // Initialiser à null ou à un tableau vide selon vos besoins
    activeModule: 1,
  },
  reducers: {
    setData: (state, action) => {
      state.products = action.payload;
    },
    setActiveModule: (state, action) => {
      state.activeModule = action.payload;
    }
  },
});

export const { setData, setActiveModule } = dataSlice.actions;
export default dataSlice.reducer;
